46cf2ddf4d0ef67c009ee90817d793badac4c2a5d3002631dd6811e9cfdff3ca
Ja som to nebol xD
Erik, hlavne klud. Je ok, ze si nahnevany. Ano, si v tomto jedna z obeti.
Ale to nijako neospravedlnuje tvoje excesy voci mne za poslednych par dni(alebo celkovo...).
Ja som s vami neuveritelne trpezlivy, ale usilovne sa blizite k tej hranici, kde to s vami
uplne vzdam a tymto veciam, co sa deju, necham absolutne volny priebeh.
A to si nezelate ani vy a momentalne si to nezelam ani ja.
Prajem pekny vikend, hlavne sa nestresuj.


Vieš, že aj ty robíš rôzne excesy voci nám za poslednych par dni(alebo celkovo...)?
A čo ich ospravedlňuje?
Vieš, že aj my sme s tebou neuveritelne trpezlivi?
Vieš, že všetci sme na pokraji hranice?
Myslíš, že keď začneš opakovať slovo "deadline", tak v nás vzbudíš pocit dôvery, pokoja a chceného uzmierenia?
My ťa nijak neprovokujeme a ak by si bol ochotný veci riešiť, tak by už boli vyriešené.
Chápeme to tak, že ti primárne ide o získavanie osobných informácií, nie o pomoc.


7af0f703cbb376c669babd5aa4cc3062e00f587f60e74f11683a2e537550eb93
(Ak ten odhad bol spravny a si Matej... A ani neviem ci tu ma zmysel pisat...)
Excesy voci vam? Ked sme sa my dvaja naposledy rozpravali, tak si ma obvinoval, ze za to mozem ja.
A voci Erikovi? Myslis tie pripomenutia, ze ma problem? To, ze to neberie vazne? Dalo by sa
to robit aj citlivejsie, ale to na veci nic nemeni - rovnako ako pred prazdninami, tak aj teraz sa
davate do toho stavu, ze sa nic pre vas nevyriesi a bude to len horsie.
V tom maily bola pravda, ja som ochotny nieco pre vas spravit len ked sa umierime
a vyriesime vzajomne problemy. Ako som ti povedal, ked sme isli k tebe na intrak:
ja som ochotny prekusnut vselico, ale ta druha strana musi prejavit dobru volu.
Ak to nie ste schopni prijat, tak staci to tu napisat, ja potom za vami nebudem viac chodit,
nebudem vam do tvare pripominat, ze vas problem stale pretrvava, nebude vam viac hovorit ze existuje
realne len jedna cesta, ako to vyriesit a to cez mier a vy si naozaj vychutnate cely neprijemny priebeh
tejto situacie, do ktorej ste sa dostali, lebo ste si znepriatelili par ludi a odmietate sa umierit s inymi.


"... vy si naozaj vychutnate cely neprijemny priebeh tejto situacie, do ktorej ste sa dostali, lebo ste si znepriatelili par ludi a odmietate sa umierit s inymi"
Znepriatelili sme si ich lebo sa dostali k nám do počítačov? Myslím, že to je normálne. Zcestné je obhajovať skôr činy, ktoré porušujú slobodu a súkromie iných.
Citlivosť podania informácie je podstatná vec pre jej akceptovanie. Všetko, čo povieš, ako to povieš a čo pri tom budeš robiť, človek vyhodnocuje a na základe toho si urobí záver.
Na veci nemení citlivosť jej podania nič, ale na jej pochopení všetko. Pravda je klamstvom, ak lož podáš z citom. ...síce to nie je správne, ale tak to je a ak to pochopíš ty, tak pochopíš, ako ťa vnímame a prečo je vzťah nalomený.
Erik sa s tebou stretol, ďalšie stretnutie by sme prijali, len ak by si mal pochopenie, ktoré nemáš, keďže odmietaš pripustiť veci, ktoré ti nespočetne veľa krát opakujeme a stále žiadaš, iné.


864d70f15142febc629738dddfab0f75ce0de7f5a79a8f33e6bfb864a08b1a39
Ja ich neobhajujem, len konstatujem, ze vas nemaju v laske, takze netusim, aky falosny dojem
sa zasa snazis vyvolat z nicoho. Halooo, uvedom si, ze bolo narusene aj moje sukromie,
nie su to moji kamarati alebo spojenci, len sa zda, ze proti vam maju nieco osobne.
Hovoris "nalomeny" vztah? Nie nalomeny, ale zlomeny, a to vy ste ho zlomili tym, ako ste sa zachovali.
Je to necestna "vyhoda", ale ked mi boli ukazane vase tajne rozhovory o mne, o ktorych ste pochopitelne
nechceli, aby som vedel, tak toto moralizovanie od teba vyznieva fakt ako cista falos. Neviem, co ti Erik hovoril
o tom stretku, ale bolo jasne, ze sa neprisiel umierit, len chcel pomoc, informacie a to je vsetko. Ked som
sa ho spytal na umierenie, tak dal jeho typicke vyhybave reci, ale tvaril sa, ze mozno hej.
Ake veci mi "nespocetne vela krat opakujete", ked sme poslednu normalnu konverzaciu mali
pred vyse 4 mesiacmi, kedy ste uverili svojim vlastnym klamstvam a vykaslali ste sa na mna?
Ak sa nechcete stretnut alebo pokial sa co i len jeden z vas nechce stretnut a uprimne sa porozpravat,
a pokusit sa umierit, tak to sem napis priamo. Nehanbi sa, neskryvaj sa za trapne reci typu:
"aj hej, ale asi radsej nie, ale by sme aj isli, ale chyba ti pochopenie, ale nic nevylucujem."
Ultimata boli dane, deadliny sa kvoli vasemu pristupu posunuli blizsie k sucasnosti(respektive je po deadline),
existuje realne len jedna cesta, ako to vyriesit a to cez mier a vy si naozaj vychutnate cely neprijemny priebeh
tejto situacie, do ktorej ste sa dostali aj tym, lebo ste si znepriatelili par ludi, ktori vas vobec nemaju v laske,
a odmietate sa umierit s inymi.


... kedy ste uverili svojim vlastnym klamstvam a vykaslali ste sa na mna? ...tvoj dojem != pravda.
... Halooo, uvedom si, ze bolo narusene aj moje sukromie, nie su to moji kamarati alebo spojenci ... skús to povedať presvedčivejšie, možno ti uveríme
... e bolo jasne, ze sa neprisiel umierit, len chcel pomoc ... rezentoval si to streto tak, že chce pomôcť, tak sa nečuduj
... existuje realne len jedna cesta, ako to vyriesit a to cez mier ... keď sa s nami nebudeš baviť, budeme mať mier. Je to to, čo chceš? Je to riešenie?
... lebo ste si znepriatelili par ludi, ktori vas vobec nemaju v laske ... mylím, že si sa dostal do štádia podobného Štokholmskému syndrómu. 
... odmietate sa umierit s inymi ... znie to dosť negatívne, ale dôvod odmietania berieme za podstatný.


d76c2e1d8adf0d32084680bed6fec1d2d1403d42d760bb653dad9e32c611b1bd
"tvoj dojem != pravda"
Ked sa uvedomite, tak sa mozeme porozpravat o nasom rozdielnom vnimani reality a urovnat to
do jednotnej podoby, co bude zrejme niekde v strede.
"skús to povedať presvedčivejšie, možno ti uveríme"
A hla, zas to zacina. Vasa paranoia vas nuti uverit svojim vlastnym klamstvam. Ja len dufam,
ze si zmiereny s tym, ze ked to bude takto pokracovat nadalej, tak vase sukromie dopadne
tak isto, ako dopadlo zaciatkom leta.
"rezentoval si to streto tak, že chce pomôcť, tak sa nečuduj"
Prepac, zla formulacia. Oprava:
Bolo jasne, ze jedine o co mal zaujem, je vyuzit moju ochotu vam pomoct, len si nebol vedomy,
ze uplna pomoc so vsetkym, co dokazem spravit, nie len s castou problemu, je podmienene dobrymi vztahmi.
"keď sa s nami nebudeš baviť, budeme mať mier. Je to to, čo chceš? Je to riešenie?"
Toto ja nechcem, ja chcem mat normalne vztahy. Ale pokial si myslis, ze budete mat mier,
ked to nedopadne medzi nami "nijako", tak si naivny hlupak. Ale beriem to tak, ze nie je pre teba
bezne viest vazne konverzacie o pol tretej rano. Skus aplikovat trochu rozmyslania a tipni si,
ako to dopadne, ked to skonci tak, ze sa medzi nami nic nenapravi.
Pokial si si tipol, ze budu nam vsetkym trom rozposielat vase sukromne informacie,
tak si asi pomerne blizko pravde. Alternativne sa bude diat nieco podobne, ako po minule 2 roky,
ked sa vam hrabali v uctoch viacmenej nepovsimnuto. Ale ked to takto vyeskalovali, tak asi je jasne, co si vyberu.
"mylím, že si sa dostal do štádia podobného Štokholmskému syndrómu."
Och, ja som len empaticky clovek a viem sa vcitit do inych. Viem, ako ste sa spravali ku mne, tusim, ako
sa Erik spraval k dalsim ludom(Terka/Majka). Aka je sanca, ze ste v minulosti poriadne nahnevali niekoho,
kto sice vam aktivne naburaval sukromie, ale teraz veri, ze ste tak zli a hnusni ludia, ze si to az zasluzite
a pokracuje v tom nadalej? Hm, vzhladom na sucasne udalosti sa ta sanca limitne blizi k 1.
"znie to dosť negatívne, ale dôvod odmietania berieme za podstatný."
Lebo ste presvedceni o svojej svatosti a uverili ste svojim klamstvam? To znie celkom blizko k pravde.
---------------------------
Len tak z cirej zvedavosti, ked si mi vtedy hovoril, ake mnohe hranice voci mne prekrocil Erik,
tak si to myslel uprimne? Alebo si len klamal, aby si ma presvedcil, ze ti mozem doverovat a ze ma chapes?
Bol by som rad, keby si odpovedal, ale mam pocit, ze sa tomu takticky vyhnes, rovnako ako k:
"Ak sa nechcete stretnut alebo pokial sa co i len jeden z vas nechce stretnut a uprimne sa porozpravat,
a pokusit sa umierit, tak to sem napis priamo."
Ocenil by som na obe uprimnu odpoved. Vies, ked poviete, ze nechcete napravit vztahy, tak sa pridam k jedeniu
popcornu, zatialco budem sledovat ten shitstorm, ktory sa rozputa, ked sa zrazu zacnu diat podivne veci.
A chcem si rovno nakupit velke zasoby na vypukanie, lebo ten shitstorm bude trvat dlho. Ber trochu ohlad aj
na ostatnych, cim skorej odpovies, tym menej mi ujde zliav.